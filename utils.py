import networkx as nx
import random as rd
import matplotlib.pyplot as plt

def random_matching(n,base_set):
    
    n = len(base_set)
    assert n%2==0

    M = []
    S = set(base_set)

    for i in range(n//2):
        s,t = rd.sample(S,2)
        M.append((s,t))
        S = S-{s,t}
    
    return M

def build_graph(res):

    G = nx.Graph()

    for (c,M) in res:

        for (u,v) in M:
            if G.has_edge(u,v):
                # print(G[u][v]["weight"])
                G[u][v]["weight"] += c
            else:
                G.add_edge(u,v,weight=c)
    return G

def random_graph(n,coeff,base_set=None):

    if base_set is None:
        base_set = list(range(n))
    
    res = [(c,random_matching(n,base_set)) for c in coeff]
    return build_graph(res)

def show_weighted_graph(G):
    pos = nx.spring_layout(G)
    nx.draw(G,pos,with_labels=True)
    edge_labels = nx.get_edge_attributes(G, 'weight')
    nx.draw_networkx_edge_labels(G, pos, edge_labels=edge_labels)
    # nx.draw_networkx_edge_labels(G,pos=nx.spring_layout(G),edge_labels='weight')
    plt.show()

def identical(G1, G2):
    # Check if node sets are equal
    if set(G1.nodes) != set(G2.nodes):
        print("not same nodes")
        return False
    
    for (u,v) in G1.edges:
        if (not G2.has_edge(u,v)):
            print("missing edge")
            return False
        if (G1[u][v]["weight"]!=G2[u][v]["weight"]):
            print("wrong weight")
            return False
    
    for (u,v) in G2.edges:
        if (not G1.has_edge(u,v)):
            print("missing edge")
            return False
        if (G1[u][v]["weight"]!=G2[u][v]["weight"]):
            print("wrong weight")
            return False
    
    return True

def is_laminar(L):

    for i in range(len(L)-1):
        for j in range(i+1,len(L)):
            if (not L[i].issubset(L[j])) and (not L[j].issubset(L[i])) and (L[i].intersection(L[j])):
                return False
    return True

def cut_value(G,S):
    val = 0
    for s in S:
        for t in G.neighbors(s):
            if t not in S:
                val += G[s][t]["weight"]
    return val
