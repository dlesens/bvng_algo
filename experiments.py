from scipy.sparse import csc_array
import networkx as nx
from scipy.io import mmread
import matplotlib.pyplot as plt
from utils import random_graph,show_weighted_graph,cut_value
from bvng import decompose
import utils
import random as rd
import sys

sys.path.append('./../')

def average(n,k,max_coeff,rep,odd_cut='rizzi',matching="bottled"):
    
    avg = 0

    for i in range(rep):
        coeff = [rd.randint(1,max_coeff) for _ in range(k)]
        print(coeff)

        G = random_graph(n,coeff)
        res = decompose(G,odd_cut=odd_cut,matching=matching)
        avg+= len(res)

    return avg/rep