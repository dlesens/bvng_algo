from scipy.sparse import csc_array
import networkx as nx
from scipy.io import mmread
import matplotlib.pyplot as plt
from utils import random_graph,show_weighted_graph,cut_value
from bvng import decompose
import utils
import random as rd
import sys
from experiments import average

sys.path.append('./../')

import cProfile
import pstats
import io
from BvN.utils.scaling import scaleNW
# from rizzi import min_odd_cut
from padberg_rao import min_odd_cut

# A = csc_array(mmread('/home/damien/.ssgetpy/MM/JGD_SPG/EX1/EX1.mtx'))
# A = csc_array(mmread('/home/damien/.ssgetpy/MM/HB/662_bus/662_bus.mtx'))
# #MUST BE SYMETRIC !!!

# A = abs(A)
# n,_ = A.shape

# A = scaleNW(A,tol=1e-4,sumtarget=1)

# g = nx.from_scipy_sparse_array(A,edge_attribute='weight')

# list_sum = []

# for v in g.nodes():
#     sum = 0
#     for u in g.neighbors(v):
#         sum+=g[u][v]["weight"]
#     list_sum.append(sum)

# print(list_sum[255])
# print(min(list_sum))

# S,val = min_odd_cut(g)
# print(len(S),n,val,S)


# print(g.edges)

# nx.draw(g)
# plt.show()

# u,v = 0,221

# print(g.get_edge_data(u, v))
# print(A[u, v])
# print(A.toarray()[:10,:10])

#for summing 2 graphs, you need to compose and then redefine edge data

#max weighted matching returns a list of tuples of nodes
#build the graph with n/2 weights everywhere and update edge weights each time the laminar family is updated (at most )
#we need to delete edges with weight 0 in all the graphs
#need a function to compress/contract/shrink a set of nodes, to do rizzi    
#need a graph generating function, just draw PM randomly and sum them, use sparse matrices to go fast maybe
# value, (S,Sb) = nx.minimum_cut(g,0,221,capacity='weight')

# print("matching")
# M = nx.max_weight_matching(g)

# print(M)
# print("cut")
# print(value, S)

n = 200
k = 40
max_coeff = 10


val1 = average(n,k,max_coeff,5,odd_cut='rizzi',matching="bottled")
print("bottled:",val1)
val2 = average(n,k,max_coeff,5,odd_cut='rizzi',matching="normal")
print("normal:",val2)


# G = nx.Graph([(28, 1, {'weight': 9}), (28, 19, {'weight': 7}), (28, 36, {'weight': 8}), (1, 33, {'weight': 7}), (1, 16, {'weight': 8}), (0, 23, {'weight': 9}), (0, 44, {'weight': 7}), (0, 19, {'weight': 8}), (23, 45, {'weight': 7}), (23, 34, {'weight': 8}), (19, 11, {'weight': 9}), (11, 43, {'weight': 7}), (11, 7, {'weight': 8}), (40, 37, {'weight': 9}), (40, 22, {'weight': 7}), (40, 14, {'weight': 8}), (37, 36, {'weight': 7}), (37, 41, {'weight': 8}), (43, 5, {'weight': 9}), (43, 15, {'weight': 8}), (5, 17, {'weight': 7}), (5, 38, {'weight': 8}), (39, 29, {'weight': 9}), (39, 27, {'weight': 7}), (39, 44, {'weight': 8}), (29, 10, {'weight': 7}), (29, 18, {'weight': 8}), (31, 41, {'weight': 9}), (31, 9, {'weight': 7}), (31, 8, {'weight': 8}), (41, 26, {'weight': 7}), (30, 35, {'weight': 9}), (30, 16, {'weight': 7}), (30, 47, {'weight': 8}), (35, 18, {'weight': 7}), (35, 45, {'weight': 8}), (34, 47, {'weight': 9}), (34, 24, {'weight': 7}), (47, 14, {'weight': 7}), (45, 9, {'weight': 9}), (9, 33, {'weight': 8}), (18, 12, {'weight': 9}), (12, 3, {'weight': 7}), (12, 26, {'weight': 8}), (33, 3, {'weight': 9}), (3, 4, {'weight': 8}), (44, 4, {'weight': 9}), (4, 46, {'weight': 7}), (14, 22, {'weight': 9}), (22, 20, {'weight': 8}), (24, 27, {'weight': 17}), (10, 6, {'weight': 9}), (10, 13, {'weight': 8}), (6, 8, {'weight': 7}), (6, 21, {'weight': 8}), (21, 49, {'weight': 9}), (21, 2, {'weight': 7}), (49, 13, {'weight': 7}), (49, 46, {'weight': 8}), (16, 25, {'weight': 9}), (25, 32, {'weight': 7}), (25, 2, {'weight': 8}), (17, 20, {'weight': 9}), (17, 48, {'weight': 8}), (20, 48, {'weight': 7}), (26, 13, {'weight': 9}), (38, 48, {'weight': 9}), (38, 15, {'weight': 7}), (36, 15, {'weight': 9}), (8, 7, {'weight': 9}), (7, 42, {'weight': 7}), (42, 46, {'weight': 9}), (42, 32, {'weight': 8}), (32, 2, {'weight': 9})])
#in this graph bottled does more work
# print(len(G.edges()))
# G = nx.Graph([(37, 41, {'weight': 6}), (37, 32, {'weight': 6}), (37, 49, {'weight': 8}), (41, 10, {'weight': 6}), (41, 15, {'weight': 8}), (0, 38, {'weight': 6}), (0, 13, {'weight': 6}), (0, 5, {'weight': 8}), (38, 1, {'weight': 6}), (38, 9, {'weight': 8}), (20, 9, {'weight': 6}), (20, 2, {'weight': 6}), (20, 3, {'weight': 8}), (9, 34, {'weight': 6}), (10, 8, {'weight': 6}), (10, 7, {'weight': 8}), (8, 26, {'weight': 6}), (8, 27, {'weight': 8}), (44, 6, {'weight': 6}), (44, 7, {'weight': 6}), (44, 16, {'weight': 8}), (6, 12, {'weight': 6}), (6, 21, {'weight': 8}), (36, 25, {'weight': 6}), (36, 35, {'weight': 6}), (36, 40, {'weight': 8}), (25, 47, {'weight': 6}), (25, 48, {'weight': 8}), (33, 3, {'weight': 6}), (33, 14, {'weight': 14}), (3, 11, {'weight': 6}), (4, 32, {'weight': 6}), (4, 43, {'weight': 6}), (4, 45, {'weight': 8}), (32, 47, {'weight': 8}), (43, 5, {'weight': 6}), (43, 26, {'weight': 8}), (5, 46, {'weight': 6}), (27, 35, {'weight': 6}), (27, 24, {'weight': 6}), (35, 11, {'weight': 8}), (30, 40, {'weight': 6}), (30, 48, {'weight': 6}), (30, 22, {'weight': 8}), (40, 45, {'weight': 6}), (17, 47, {'weight': 6}), (17, 15, {'weight': 6}), (17, 2, {'weight': 8}), (42, 16, {'weight': 6}), (42, 49, {'weight': 6}), (42, 1, {'weight': 8}), (16, 28, {'weight': 6}), (24, 49, {'weight': 6}), (24, 34, {'weight': 8}), (28, 29, {'weight': 6}), (28, 12, {'weight': 8}), (29, 22, {'weight': 6}), (29, 13, {'weight': 8}), (7, 46, {'weight': 6}), (46, 18, {'weight': 8}), (34, 2, {'weight': 6}), (21, 13, {'weight': 6}), (21, 23, {'weight': 6}), (31, 19, {'weight': 14}), (31, 39, {'weight': 6}), (19, 18, {'weight': 6}), (22, 39, {'weight': 6}), (39, 23, {'weight': 8}), (18, 26, {'weight': 6}), (11, 48, {'weight': 6}), (14, 15, {'weight': 6}), (45, 23, {'weight': 6}), (1, 12, {'weight': 6})])
#this graph blocked once but can't reproduce it

# G1 = random_graph(n,coeff,base_set=list(range(n)))
# G2 = random_graph(n,coeff,base_set=list(range(n,2*n)))
# # G = nx.union(G1,G2,rename=("1-","2-"))
# G = nx.union(G1,G2)


# print(G.edges(data=True))
# G = nx.Graph([(5, 4, {'weight': 1}), (5, 6, {'weight': 3}), (5, 1, {'weight': 5}), (5, 3, {'weight': 2}), (4, 0, {'weight': 8}), (4, 2, {'weight': 2}), (8, 7, {'weight': 6}), (8, 1, {'weight': 3}), (8, 0, {'weight': 2}), (7, 3, {'weight': 3}), (7, 1, {'weight': 2}), (9, 1, {'weight': 1}), (9, 2, {'weight': 3}), (9, 6, {'weight': 7}), (0, 3, {'weight': 1}), (3, 2, {'weight': 5}), (6, 2, {'weight': 1})])

# print(G.edges.data())
# show_weighted_graph(G)

# pr = cProfile.Profile()
# pr.enable()






# coeff = [rd.randint(1,max_coeff) for _ in range(k)]

# G = random_graph(n,coeff)

# res = decompose(G,odd_cut='rizzi',matching="bottled")

# Gres = utils.build_graph(res)
# print(utils.identical(Gres,G))
# print(len(res))

# res = decompose(G,odd_cut='rizzi',matching="normal")

# Gres = utils.build_graph(res)

# # print(res)
# print(utils.identical(Gres,G))
# # print(G.edges.data())
# # print(Gres.edges.data())
# print(len(res))







# pr.disable()

# # Print profiling results
# s = io.StringIO()
# ps = pstats.Stats(pr, stream=s).sort_stats(pstats.SortKey.CUMULATIVE)
# ps.print_stats()

# print(s.getvalue())

# pr = cProfile.Profile()
# pr.enable()

# res = decompose(G,odd_cut='padberg')

# Gres = utils.build_graph(res)

# # print(res)
# print(utils.identical(Gres,G))
# # print(G.edges.data())
# # print(Gres.edges.data())
# print(len(res))

# pr.disable()

# # Print profiling results
# s = io.StringIO()
# ps = pstats.Stats(pr, stream=s).sort_stats(pstats.SortKey.CUMULATIVE)
# ps.print_stats()

# print(s.getvalue())
"""
In this report, I presented a new family of heuristics for the sparse BvN decomposition of doubly-stochastic matrices. The proposed heuristics are based on the well-known greedy orthogonal matching pursuit (OMP) algorithm,
and advances the state of the art in the BvN decomposition problem.
Experimental results show that the proposed heuristics overcome the innate limitation of
the state of the art approaches in the literature and are competitive with them
on general instances. Interesting future directions would be to find limitations for these heuristics and find other ways to 

I also presented an amelioration of the only known algorithm for the generalisation of the BvN decomposition to non-bipartite graphs. I greatly improved the theoretical complexity and produced the first implementation for this problem. Future works would be to further develop the algorithm so that it can be used on real world symmetric matrices.
"""