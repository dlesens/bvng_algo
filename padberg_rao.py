import networkx as nx
import numpy as np
    


def min_odd_cut(G):

    T = nx.gomory_hu_tree(G,capacity='weight',flow_func=nx.algorithms.flow.preflow_push)

    T0 = T.copy()

    list_min_edges = []

    list_trees = [T0]

    while list_trees:

        T1 = list_trees.pop()

        min_edge = min(T1.edges(data=True), key=lambda x: x[2]['weight'])

        T1.remove_edge(min_edge[0],min_edge[1])

        comps = list(nx.connected_components(T1))

        if len(comps[0])%2 == 1:
            list_min_edges.append(min_edge)
        else:
            for c in comps:
                list_trees.append(T1.subgraph(c).copy())
    
    final_edge = min(list_min_edges,key=lambda x: x[2]['weight'])

    T.remove_edge(final_edge[0],final_edge[1])
    comps = list(nx.connected_components(T))
    # print(final_edge)
    return comps[0],final_edge[2]["weight"]

