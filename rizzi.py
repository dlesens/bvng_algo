import networkx as nx
import random as rd

#to go faster, use maps instead of sets because the ground set is size n
#code a function which returns the cut, usefull later for uncrossing -> not really actually

def contract(G,S):
    #S is a subset of the nodes of G
    #returns graphs G/S and G/Sb
    #the node representing the contracted sets will be returned for indentification purposes
    new_node = max(G.nodes()) + 1#for now,
    # new_node = next(iter(S))

    #maybe not very efficient for now
    Sb = set(G.nodes())-S

    GcS = G.copy()
    GcSb = G.copy()

    crossing = []
    
    for s in S:
        for v in G.neighbors(s):
            if v not in S:
                crossing.append((s,v))
    
    #building G/S and G/Sb
    for s in S:
        GcS.remove_node(s)
    for s in Sb:
        GcSb.remove_node(s)
    
    GcS.add_node(new_node)
    GcSb.add_node(new_node)

    for u,v in crossing:
        #u is in S and v in V-S
        if GcS.has_edge(new_node,v):
            GcS[new_node][v]['weight'] += G[u][v]['weight']
        else:
            GcS.add_edge(new_node,v,weight=G[u][v]['weight'])
        
        if GcSb.has_edge(new_node,u):
            GcSb[new_node][u]['weight'] += G[u][v]['weight']
        else:
            GcSb.add_edge(new_node,u,weight=G[u][v]['weight'])

    return GcS,GcSb,new_node

def contract_st(G,s,t):
    new_node = max(G.nodes()) + 1

    Gcst = G.copy()

    Gcst.remove_node(s)
    Gcst.remove_node(t)
    Gcst.add_node(new_node)

    for u in G.neighbors(s):
        if u != t:
            if Gcst.has_edge(new_node,u):
                Gcst[new_node][u]['weight'] += G[s][u]['weight']
            else:
                Gcst.add_edge(new_node,u,weight=G[s][u]['weight'])
    
    for u in G.neighbors(t):
        if u != s:
            if Gcst.has_edge(new_node,u):
                Gcst[new_node][u]['weight'] += G[t][u]['weight']
            else:
                Gcst.add_edge(new_node,u,weight=G[t][u]['weight'])

    return Gcst,new_node

def min_t_cut(G,T):
    #G is a weighted graph and T is even
    if len(T)==0:
        return set(),None#None represents +infinity

    list_nodes = list(G.nodes)

    s,t = rd.sample(T,2)

    value_st, (S,Sb) = nx.minimum_cut(G,s,t,capacity='weight')

    ScapT = S.intersection(T)

    if len(ScapT) % 2 == 1:#S is T odd
        Tmst = T-{s,t}

        Gcst,new_node = contract_st(G,s,t)

        cutmst,valuemst = min_t_cut(Gcst,Tmst)

        if new_node in cutmst:
            cutmst.remove(new_node)
            cutmst = cutmst.union({s,t})

        if valuemst is None:
            mincut,minval = S,value_st
        elif value_st is None:
            mincut,minval = cutmst,valuemst
        elif valuemst >= value_st:
            mincut,minval = S,value_st
        else:
            mincut,minval = cutmst,valuemst
        
        return mincut,minval


    else:#S is T even
        TmS = T-S
        TmSb = T-Sb

        GcS,GcSb,new_node = contract(G,S)

        cutS,valueS = min_t_cut(GcS,TmS)
        cutSb,valueSb = min_t_cut(GcSb,TmSb)

        if valueSb is None:
            mincut,minval = cutS,valueS
            nodeset = S
        elif valueS is None:
            mincut,minval = cutSb,valueSb
            nodeset = Sb
        elif valueS >= valueSb:
            mincut,minval = cutSb,valueSb
            nodeset = Sb
        else:
            mincut,minval = cutS,valueS
            nodeset = S

        if new_node in mincut:
            mincut.remove(new_node)
            mincut = mincut.union(nodeset)
        
        return mincut,minval
    
def min_odd_cut(G):
    return min_t_cut(G,set(G.nodes))