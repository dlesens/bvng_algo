import networkx as nx
import rizzi
import padberg_rao
from utils import show_weighted_graph
import utils
from bottled_binary import max_weight_matching_bottled

#optim composantes connexes et graphe biparti

def decompose(G,odd_cut="rizzi",matching="bottled"):
    algo = BvNG(G,odd_cut)
    algo.compute(matching=matching)
    return algo.result


class BvNG:

    def __init__(self,Ginit,odd_cut):
        #G is a weighted graph, which represents a fractional perfect matching
        #assumes that the nodes are numbered from 0 to n-1
        self.n = Ginit.number_of_nodes()
        self.L = []#laminar family
        self.Gmatch = Ginit.copy()
        #init the coefficients
        for (u,v) in Ginit.edges():
            self.Gmatch[u][v]["weight"]=self.n//2
        
        self.result = []#will contain couples (coefficient,matching)
        self.G = Ginit.copy()
        
        any_node = list(Ginit.nodes)[0]
        self.alpha = sum([Ginit[any_node][u]["weight"] for u in Ginit.neighbors(any_node)])#summing the weights adjacent to an arbitrary vertex

        self.m = Ginit.size()
        self.V = self.G.nodes

        match odd_cut:
            case 'rizzi':
                self.min_odd_cut=rizzi.min_odd_cut
            case 'padberg':
                self.min_odd_cut=padberg_rao.min_odd_cut

        return

    def substractG(self,M,coeff):
        Gp = self.G.copy()
        for (u,v) in M:
            # print(self.G[u][v]["weight"])
            Gp[u][v]["weight"] -= coeff
            # print(self.G[u][v]["weight"])
        return Gp
    
    def cut_value_G(self,S):
        val = 0
        for s in S:
            for t in self.G.neighbors(s):
                if t not in S:
                    val += self.G[s,t]["weight"]
        return val
    
    def update_graphs(self,M,beta):
        for (u,v) in M:
            if self.G[u][v]['weight']==0:
                self.G.remove_edge(u,v)
                self.Gmatch.remove_edge(u,v)
                self.m -= 1

    def binary_search(self,M,beta):
        #we know that beta already doesn't work
        lower = 0 #inclusive
        upper = beta #exclusive
        while True:
            mid = (upper+lower)/2
            Gp = self.substractG(M,mid)
            S,valueS = self.min_odd_cut(Gp)#what if S is crossed only once ?
            #compute cut value and how much it is crossed by M
            McrossS = self.count_cross_M(S,M)

            if McrossS==1:
                lower = mid
                continue

            cut_val_G = valueS + McrossS*mid#self.cut_value_G(S) should do right ?
            gamma = (cut_val_G - self.alpha)/(McrossS-1)

            if gamma==0:
                return 0,S
            #compute the cut value with gamma
            cut_val_gamma = cut_val_G-McrossS*gamma

            if cut_val_gamma==self.alpha-gamma:
                return gamma,S
            
            if valueS < self.alpha-mid:
                upper = gamma
            else:
                lower = gamma



    def count_cross_M(self,S,M):
        count=0
        for (u,v) in M:
            if ((u in S)and(v not in S))or((u not in S)and(v in S)):
                count+=1
        return count
    
    def updateL(self,S,M):
        """
        need to:
        -uncross S
        -add it to L
        -update w(L)
        """
        S0 = S.copy()
        for T in self.L:
            TcapS = T.intersection(S)
            TmS = T-S
            SmT = S-T
            TcupS = T.union(S)
            if S==T:
                print("repetition")
                return
            if (TcapS) and (TmS) and (SmT):
                print("UNCROSSED !!!!!!!!!!!!!!!")
                if len(TcapS) % 2 == 1:
                    #uncross to ScupT and ScapT
                    if self.n-len(TcupS)==1:
                        S = TcapS
                        continue
                    if len(TcapS)==1:
                        S = TcupS
                        continue
                    if self.count_cross_M(TcapS,M)>1:
                        S = TcapS
                        continue
                    else:
                        S = TcupS
                        continue

                else:
                    #uncross to S-T and T-S
                    if len(TmS)==1:
                        S = SmT
                        continue
                    if len(SmT)==1:
                        S = TmS
                        continue
                    if self.count_cross_M(SmT,M)>1:
                        S = SmT
                        continue
                    else:
                        S = TmS
                        continue
        if S0==S:
            print("didnt uncross :(")
        #S can now be added to L
        self.L.append(S)
        print("added set of size",len(S))
        for (u,v) in self.G.edges:
            if ((u in S)and(v not in S))or((u not in S)and(v in S)):
                self.Gmatch[u][v]["weight"]-=1
        return
    
    def compute(self,max_iter=None,matching="bottled"):

        if max_iter is None:
            max_iter = self.m
        
        iter = 0

        while self.m > 0 and iter < max_iter:

            match matching:
                case "normal":
                    M = list(nx.max_weight_matching(self.Gmatch,maxcardinality=True))#can use not weighted matching if L = emptyset
                case "bottled":
                    M = list(max_weight_matching_bottled(self.G,self.Gmatch,len(self.L)))


            beta = min([self.G[u][v]["weight"] for (u,v) in M])
            # print("beta",beta)

            Gp = self.substractG(M,beta)
            # show_weighted_graph(Gp)
            # print(Gp.edges.data())
            # print(self.G.edges.data())

            S,valueS = self.min_odd_cut(Gp)#it is the bottleneck

            if valueS >= self.alpha - beta:
                print(".",end='',flush=True)
                self.result.append((beta,M))
                iter+=1
                self.G = Gp.copy()
                self.alpha -= beta
                self.update_graphs(M,beta)
                # show_weighted_graph(self.G)
            
            else:
                print("do more work")
                gamma,S = self.binary_search(M,beta)

                if gamma > 0:
                    print("positive gamma")
                    self.result.append((gamma,M))
                    iter+=1
                    self.G = self.substractG(M,gamma)
                    self.alpha -= gamma
                    #no update
                
                self.updateL(S,M)
        print(self.L)
        print("is L laminar ?",utils.is_laminar(self.L))
        print("fini")


            
        


        
