import networkx as nx

def max_weight_matching_bottled(G,Gmatch,sizeL):
    #sizeL used so now if correct matching (value should be n(n//2-sizeL))
    #do matching in Gmatch but look at edge weight in 
    n = len(G.nodes())

    edge_values0 = list(set([G[u][v]["weight"] for (u,v) in G.edges()]))

    edge_values = edge_values0.copy()

    while len(edge_values)>1:

        thrs = edge_values[len(edge_values)//2]

        Gthrs = nx.Graph()
        Gthrs.add_edges_from((u,v,data) for u,v,data in Gmatch.edges(data=True) if G[u][v]["weight"]>=thrs)

        M = nx.max_weight_matching(Gthrs,maxcardinality=True)

        valueM = sum([Gmatch[u][v]["weight"] for (u,v) in M])

        if valueM < n*(n//2-sizeL):#includes the case when the matching is not perfect
            edge_values = edge_values[:len(edge_values)//2]
        else:
            edge_values = edge_values[len(edge_values)//2+1]
    
    #the bottleneck value is the one before edge_values[0]
    id = edge_values0.index(edge_values[0])-1

    Gthrs = nx.Graph()
    Gthrs.add_edges_from((u,v,data) for u,v,data in Gmatch.edges(data=True) if G[u][v]["weight"]>=edge_values0[id])

    M = nx.max_weight_matching(Gthrs,maxcardinality=True)
    return M

    



